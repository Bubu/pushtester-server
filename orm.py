import enum

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Priority(enum.Enum):
    normal = 0
    high = 1


class NotificationPriority(enum.Enum):
    Minimum = 0
    Low = 1
    Default = 2
    High = 3
    Maximum = 4


class Notification(db.Model):
    __tablename__ = "message"
    id = db.Column(db.Integer, primary_key=True)
    pushserver = db.Column(db.String(255), nullable=False)
    token = db.Column(db.String(80), nullable=False)
    scheduled_time = db.Column(db.DateTime, nullable=False)
    priority = db.Column(db.Enum(Priority), nullable=False)
    notification_priority = db.Column(db.Enum(NotificationPriority), nullable=False)
    retry_count = db.Column(db.Integer, nullable=False)

    def as_dict(self, timestamp):
        return {
            "data": {
                "title": "Test notification",
                "serverTime": timestamp,
                "notificationPrio": self.notification_priority.value,
            },
            "token": self.token,
            "priority": self.priority.name,
            "time_to_live": 86400,
        }
