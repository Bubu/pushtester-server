import requests


def search(pushserver=None):
    if not pushserver:
        return 1
    try:
        if not (pushserver.startswith("http://") or pushserver.startswith("https://")):
            raise requests.exceptions.InvalidSchema
        r = requests.get(pushserver + "/version", timeout=(0.5, 0.5))
        r.raise_for_status()
        # some api version plausibility check
        try:
            if r.json()["version"] is not "":
                return 1
            else:
                return 404
        except TypeError:
            return 404
    except requests.exceptions.RequestException as e:
        return str(e.__class__), 404
