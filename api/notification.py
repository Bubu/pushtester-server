from datetime import datetime, timedelta
from orm import db, Notification, Priority, NotificationPriority

from sqlalchemy.exc import SQLAlchemyError
from app import scheduler
from flask.helpers import get_debug_flag
import requests
from time import time

import app
import configs


def post_to_pushserver(notfification):
    r = requests.post(
        notfification.pushserver + "/message", json=notfification.as_dict(time())
    )
    r.raise_for_status()


def deliver_notifications():
    now = datetime.now()
    config = configs.DevConfig if get_debug_flag() else configs.ProdConfig
    with app.create_app(config).app.app_context():
        session = db.sessionmaker(bind=db.get_engine())()
        notifications = session.query(Notification).filter(
            Notification.scheduled_time <= now
        ).all()
        for notification in notifications:
            try:
                post_to_pushserver(notification)
                session.delete(notification)
                session.commit()
            except Exception as e:
                print(str(e))
                if notification.retry_count < 2:
                    notification.retry_count += 1
                    scheduler.add_job(deliver_notifications, "date", run_date=datetime.now() + timedelta(seconds=5))
                else:
                    session.delete(notification)
                    session.commit()
        session.close()


def search(pushserver, pushId, delay=0, deliveryPrio=0, notificationPrio=0):
    priority = Priority(deliveryPrio)
    notification_priority = NotificationPriority(notificationPrio)
    senddate = datetime.now() + timedelta(seconds=delay)
    notification = Notification(
        pushserver=pushserver,
        priority=priority,
        notification_priority=notification_priority,
        token=pushId,
        scheduled_time=senddate,
        retry_count=0,
    )
    try:
        db.session.add(notification)
        db.session.commit()
    except SQLAlchemyError as e:
        print(str(e))
        return 0, 400
    scheduler.add_job(deliver_notifications, "date", run_date=senddate)
    return 1
